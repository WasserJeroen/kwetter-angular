import {User} from './user';

export class Kweet {
  public id: string;
  public message: string;
  public date: string;
  public author: User;
  public showDate: Date;
}
