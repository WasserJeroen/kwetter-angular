export class User {
  public id: string;
  public userName: string;
  public password: string;
  public bio: string;
  public email: string;
  public location: string;
  public photo: string;
  public website: string;
  public followers: [];
  public following: [];
  public actualFollowers: User[];
  public actualFollowing: User[];
  public token?: string;
}
