import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {first, map} from 'rxjs/operators';
import {User} from '../_models';
import {tap} from 'rxjs/internal/operators';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  public currentToken: Observable<string>;
  private currentTokenSubject: BehaviorSubject<string>;
  private selectedUserSubject: BehaviorSubject<User>;
  public selectedUser: Observable<User>;
  public token: string;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.currentTokenSubject = new BehaviorSubject<string>(localStorage.getItem('currentToken'));
    this.currentToken = this.currentTokenSubject.asObservable();
    this.selectedUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('selectedUser')));
    this.selectedUser = this.selectedUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public set selectedUserValue(user: User) {
    localStorage.removeItem('selectedUser');
    this.selectedUserSubject.next(null);
    localStorage.setItem('selectedUser', JSON.stringify(user));
    this.selectedUserSubject.next(user);
  }

  public get currentTokenValue(): string {
    return this.currentTokenSubject.value;
  }

  login(username: string, password: string) {
    const params = new HttpParams()
      .set('name', username)
      .set('password', password)
      .set('rememberme', 'false');
    return this.http.get('http://localhost:8080/Kwetter-1.0-SNAPSHOT/api/auth/login?',{ params, observe: 'response' })
      .pipe(map(user => {
        this.token = user.headers.get('Authorization');
        // login successful if there's a jwt token in the response
        if (this.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentToken', this.token);
          this.currentTokenSubject.next(this.token);
          // this.currentUserSubject.next(user);
        }
        this.getLoggedInUser(username, password).pipe(first())
          .subscribe(
            data => {
              localStorage.setItem('currentUser', JSON.stringify(data));
              this.currentUserSubject.next(data);
              localStorage.setItem('selectedUser', JSON.stringify(data));
              this.selectedUserSubject.next(data);
              // console.log(data);
              return data;
            });
        return user;
      }));
    }

    getLoggedInUser(userName: string, password: string) {
      const params = new HttpParams()
        .set('userName', userName)
        .set('password', password);
      return this.http.get<User>('http://localhost:8080/Kwetter-1.0-SNAPSHOT/api/users/getLoggedIn',{ params })
        .pipe(map(user => {
          return user;
        }));
    }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentToken');
    this.currentTokenSubject.next(null);
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
