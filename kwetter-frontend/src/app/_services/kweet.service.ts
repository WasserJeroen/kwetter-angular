import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Kweet, User} from '../_models';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';


@Injectable({ providedIn: 'root' })
export class KweetService {
  constructor(private http: HttpClient) {
  }

  getAllFromUser(userId: string) {
    const params = new HttpParams()
      .set('userId', userId);
    return this.http.get<Kweet[]>('http://localhost:8080/Kwetter-1.0-SNAPSHOT/api/kweets/kweet?', {params});
  }

  getAllKweets() {
    return this.http.get<Kweet[]>('http://localhost:8080/Kwetter-1.0-SNAPSHOT/api/kweets')
      .pipe();
  }

  placeKweet(author: User, kweet: Kweet) {
    const params = new HttpParams()
      .set('userId', author.id);
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    return this.http.post<Kweet>('http://localhost:8080/Kwetter-1.0-SNAPSHOT/api/kweets?', kweet, {params, headers})
      .pipe();
  }
}

