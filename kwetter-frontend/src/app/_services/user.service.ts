import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {User} from '../_models';


@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<User[]>('http://localhost:8080/Kwetter-1.0-SNAPSHOT/api/users');
  }

  getUser(id: string) {
    const params = new HttpParams()
      .set('userId', id);
    return this.http.get<User>('http://localhost:8080/Kwetter-1.0-SNAPSHOT/api/users/user?', {params});
  }

  followUser(loggedInUser: string, userToBeFollowed: string, token: string) {
    const params = new HttpParams()
      .set('userId', userToBeFollowed)
      .set('idToFollow', loggedInUser);
    const headers = new HttpHeaders()
      .set('Authorization', token);
    return this.http.put<User>('http://localhost:8080/Kwetter-1.0-SNAPSHOT/api/users/follow?', {params}).pipe();
  }
}
