import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend

import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './home/home.component';
import {routing} from './app.router';
import {MatCardModule} from '@angular/material/card';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {TimelineComponent} from './components/timeline/timeline.component';
import {ProfileComponent} from './components/profile/profile.component';
import {FilterPipe} from './pipes/filterpipe';
import {MatIconModule, MatListModule, MatTableModule} from '@angular/material';
import {ScrollingModule} from '@angular/cdk/scrolling';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    routing,
    MatCardModule,
    NoopAnimationsModule,
    FlexLayoutModule,
    NgZorroAntdModule,
    FormsModule,
    MatListModule,
    MatIconModule,
    ScrollingModule,
    MatTableModule,
    BrowserModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    TimelineComponent,
    ProfileComponent,
    FilterPipe,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
