import { Component } from '@angular/core';
import { first } from 'rxjs/operators';
import {Kweet, User} from '../../_models';
import {UserService} from '../../_services/user.service';
import {KweetService} from '../../_services/kweet.service';
import {ActivatedRoute} from '@angular/router';
import {AuthenticationService} from '../../_services/authentication.service';


@Component({ templateUrl: 'profile.component.html' })
export class ProfileComponent {
  users: User[] = [];
  kweets: Kweet[] = [];
  selectedUser: User;
  loggedInUser: User;
  sub: any;
  constructor(private userService: UserService, private kweetService: KweetService, private route: ActivatedRoute, private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.authenticationService.selectedUser.subscribe(user => this.loggedInUser = user);

    this.sub = this.route.params.subscribe(params => {
      this.userService.getUser(params.id).pipe().subscribe(user => {
        this.selectedUser = user;
        this.selectedUser.actualFollowing = [];
        this.selectedUser.actualFollowers = [];
        for (const val of this.selectedUser.followers) {
          this.userService.getUser(val).pipe().subscribe(user => {
            console.log(user);
            this.selectedUser.actualFollowers.push(user);
          });
        }
        for (const val of this.selectedUser.following) {
          this.userService.getUser(val).pipe().subscribe(user => {
            console.log(user);
            this.selectedUser.actualFollowing.push(user);
          });
        }
        this.kweetService.getAllFromUser(this.selectedUser.id).pipe().subscribe(kweets => {
          this.kweets = kweets;
          for (const val of this.kweets) {
            val.showDate = new Date(val.date);
            console.log(val.showDate);
          }
        });
      });

      // In a real app: dispatch action to load the details here.
    });
  }
  followUser() {
    console.log(this.selectedUser);
    console.log(this.loggedInUser);
    this.userService.followUser(this.loggedInUser.id, this.selectedUser.id, this.authenticationService.currentTokenValue).pipe().subscribe(niks => {
      console.log(niks);
    });
  }
}
