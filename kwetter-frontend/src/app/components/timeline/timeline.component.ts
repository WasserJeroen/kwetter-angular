import {Component} from '@angular/core';
import {AuthenticationService} from '../../_services/authentication.service';
import {UserService} from '../../_services/user.service';
import {KweetService} from '../../_services/kweet.service';
import {Kweet} from '../../_models/kweet';
import {first} from 'rxjs/operators';
import {User} from '../../_models';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {formatDate} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';

@Component({ templateUrl: 'timeline.component.html' })
export class TimelineComponent {
  kweets: Kweet[] = [];
  postKweetForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  loggedInUser: User;
  toBePlaced: Kweet;
  searchText: string;


  constructor(private userService: UserService, private authenticationService: AuthenticationService, private kweetService: KweetService, private formBuilder: FormBuilder, private router: Router,
  ) {
  }
  ngOnInit() {
    this.postKweetForm = this.formBuilder.group({
      message: ['', Validators.required]
    });

    this.authenticationService.currentUser.subscribe(user => this.loggedInUser = user);
    console.log(this.loggedInUser);
    this.kweetService.getAllKweets().pipe().subscribe(kweets => {
      this.kweets = kweets;
      for (const val of this.kweets) {
        val.showDate = new Date(val.date);
        console.log(val.showDate);
      }
    });
  }
  get f() { return this.postKweetForm.controls; }

  onSubmit() {
    this.submitted = true;

    this.toBePlaced = new Kweet();
    // stop here if form is invalid
    if (this.postKweetForm.invalid) {
      return;
    }

    this.loading = true;
    this.toBePlaced.message = this.f.message.value;
    this.toBePlaced.date = formatDate(new Date(), 'yyyy-MM-ddTHH:mm', 'en');

    console.log(this.toBePlaced);
    this.kweetService.placeKweet(this.loggedInUser, this.toBePlaced)
      .pipe(first())
      .subscribe(
        data => {
          console.log('test');
          this.toBePlaced = null;
          this.loading = false;
          this.kweets.push(data);
          this.router.navigate(['timeline']);
        });
  }

  goToProfile(id) {
    console.log(id);
    this.router.navigate(['/profile', id]);
  }
}
