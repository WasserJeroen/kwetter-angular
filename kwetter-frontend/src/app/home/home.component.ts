import { Component } from '@angular/core';
import { first } from 'rxjs/operators';
import {User} from '../_models';
import {UserService} from '../_services/user.service';
import {AuthenticationService} from '../_services/authentication.service';
import {Kweet} from '../_models/kweet';
import {KweetService} from '../_services/kweet.service';


@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
  users: User[] = [];
  kweets: Kweet[] = [];
  loggedInUser: User;
  columnsToDisplay = ['followers'];
  i = 0;
  constructor(private userService: UserService, private authenticationService: AuthenticationService, private kweetService: KweetService) {
  }

  ngOnInit() {
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.users = users;
    });
    // this.loggedInUser = this.authenticationService.currentUserValue;
    this.authenticationService.selectedUser.subscribe(user => this.loggedInUser = user);
    console.log(this.loggedInUser);
    this.loggedInUser.actualFollowers = [];
    this.loggedInUser.actualFollowing = [];
    this.kweetService.getAllFromUser(this.loggedInUser.id).pipe().subscribe(kweets => {
      this.kweets = kweets;
      for (const val of this.kweets) {
        val.showDate = new Date(val.date);
        console.log(val.showDate);
      }
    });
    for (const val of this.loggedInUser.followers) {
      this.userService.getUser(val).pipe().subscribe(user => {
        console.log(user);
        this.loggedInUser.actualFollowers.push(user);
      });
    }
    for (const val of this.loggedInUser.following) {
      this.userService.getUser(val).pipe().subscribe(user => {
        console.log(user);
        this.loggedInUser.actualFollowing.push(user);
      });
    }
  }
}
